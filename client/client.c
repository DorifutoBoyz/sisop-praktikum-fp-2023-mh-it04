#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define SERVER_IP "127.0.0.1"
#define PORT 8080
#define textCmd_SIZE 1024

int main(int argc, char *argv[]) {
    int isRoot = 1;
    char *sudo_user = getenv("SUDO_USER");
    char *uName = NULL;

    if (sudo_user == NULL) {
        isRoot = 0;
        if (argc != 5) {
            printf("Invalid number of arguments.\n");
            return 1;
        }

        char *uPass = NULL;

        for (int i = 1; i < argc; i++) {
            if (strcmp(argv[i], "-u") == 0 && i + 1 < argc) {
                uName = argv[i + 1];
                i++;  
            } else if (strcmp(argv[i], "-p") == 0 && i + 1 < argc) {
                uPass = argv[i + 1];
                i++; 
            }
        }

        if (uName == NULL || uPass == NULL) {
            printf("Invalid arguments.\n");
            return 1;
        }

        FILE *file = fopen("../database/databases/users/user_list.txt", "r");
        if (file != NULL) {
            char line[256];
            fgets(line, sizeof(line), file); 
            int isAuthorized = 0;

            while (fgets(line, sizeof(line), file) != NULL) {
                char *trimmedLine = line;
                size_t lineLength = strlen(trimmedLine);

                while (lineLength > 0 && (trimmedLine[lineLength - 1] == '\n' || trimmedLine[lineLength - 1] == ' ')) {
                    trimmedLine[--lineLength] = '\0';
                }

                char *username = strtok(trimmedLine, " -");
                char *password = strtok(NULL, " -");

                if (strcmp(username, uName) == 0 && strcmp(password, uPass) == 0) {
                    isAuthorized = 1;
                    break;
                }
            }

            fclose(file);

            if (!isAuthorized) {
                printf("Invalid username or password.\n");
                return 1;
            }
        } 
        
        else {
            printf("User list file not found.\n");
            return 1;
        }

        isRoot = 0;

    } else {
        uName = "root";
    }

    int clSocket;
    struct sockaddr_in srvAddr;
    char textCmd[textCmd_SIZE];

    clSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (clSocket == -1) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    srvAddr.sin_family = AF_INET;
    srvAddr.sin_port = htons(PORT);
    if (inet_pton(AF_INET, SERVER_IP, &(srvAddr.sin_addr)) <= 0) {
        perror("Invalid address/Address not supported");
        exit(EXIT_FAILURE);
    }

    if (connect(clSocket, (struct sockaddr*)&srvAddr, sizeof(srvAddr)) == -1) {
        perror("Connection failed");
        exit(EXIT_FAILURE);
    }

    send(clSocket, uName, strlen(uName), 0);

    while (1) {
        printf("Enter a message (or 'q' to quit): ");
        fgets(textCmd, sizeof(textCmd), stdin);

        send(clSocket, textCmd, strlen(textCmd), 0);

        if (textCmd[0] == 'q')
            break;

        memset(textCmd, 0, sizeof(textCmd));
        recv(clSocket, textCmd, sizeof(textCmd), 0);
    }

    close(clSocket);
    return 0;
}

