#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 8080

char *uPass = NULL;
char *uName = NULL;

int main(int argc, char *argv[]){
    int isRoot = 1;
    char *sudo_user = getenv("SUDO_USER");

    if (sudo_user == NULL) {
        isRoot = 0;
        if (argc != 6) {
            printf("Invalid number of arguments.\n");
            return 1;
        }

        for (int i = 1; i < argc; i++) {
            if (strcmp(argv[i], "-u") == 0 && i + 1 < argc) {
                uName = argv[i + 1];
                i++;  
            } else if (strcmp(argv[i], "-p") == 0 && i + 1 < argc) {
                uPass = argv[i + 1];
                i++; 
            }
        }

        if (uName == NULL || uPass == NULL) {
            printf("Invalid arguments.\n");
            return 1;
        }

        FILE *file = fopen("../database/databases/users/user_list.txt", "r");
        if (file != NULL) {
            char line[256];
            fgets(line, sizeof(line), file); 
            int isAuthorized = 0;

            while (fgets(line, sizeof(line), file) != NULL) {
                char *trimmedLine = line;
                size_t lineLength = strlen(trimmedLine);

                while (lineLength > 0 && (trimmedLine[lineLength - 1] == '\n' || trimmedLine[lineLength - 1] == ' ')) {
                    trimmedLine[--lineLength] = '\0';
                }

                char *username = strtok(trimmedLine, " -");
                char *password = strtok(NULL, " -");

                if (strcmp(username, uName) == 0 && strcmp(password, uPass) == 0) {
                    isAuthorized = 1;
                    break;
                }
            }

            fclose(file);

            if (!isAuthorized) {
                printf("Invalid username or password.\n");
                return 1;
            }
        } 
        
        else {
            printf("User list file not found.\n");
            return 1;
        }

        isRoot = 0;

    } else {
        uName = "root";
    }

    FILE *fp, *dp;
    fp = fopen("../database/log.txt", "r");
    dp = fopen("command_backup.txt", "w");
    if (fp == NULL) {
        printf("Failed to open log file.\n");
        return 0;
    }
    char buffer[20000];
    while (fgets(buffer, sizeof(buffer), fp)) {
        char *colon = strchr(buffer, ':');
        if (colon != NULL) {
            colon = strchr(colon + 1, ':');
            colon = strchr(colon + 1, ':');
            colon = strchr(colon + 1, ':');
            if (colon != NULL) {
                fprintf(dp, "%s", colon + 1);
            }
        }
    }
    
    fclose(fp);
    fclose(dp);

    return 0;
}


