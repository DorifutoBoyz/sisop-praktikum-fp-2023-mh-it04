#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>
#include <errno.h>

#define PORT 8080
#define MAX_CLIENTS 5
#define FILENAME "databases/users/user_list.txt"

char basePath[1024] = "databases";
char uName[256];

int check_access(const char *uname) {
    FILE *file = fopen(FILENAME, "r");
    if (file == NULL) {
        return -1;
    }

    char line[1024];
    int hasAccess = 0;  

    while (fgets(line, sizeof(line), file)) {
        char *found = strstr(line, uname);
        if (found != NULL) {
            char *access = strstr(found, "access");
            if (access != NULL) {
                hasAccess = 1;
                break;
            }
        }
    }

    fclose(file);

    return hasAccess;
}

void write_log(const char *uname, const char *command) {
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm);

    FILE *log_file = fopen("log.txt", "a");
    if (log_file) {
        fprintf(log_file, "%s:%s:%s\n", timestamp, uname, command);
        fclose(log_file);
    }
}

int directoryExists(const char* dirPth) {
    if (access(dirPth, F_OK) == -1) {
        return 0; 
    }
    
    return 1; 
}

int fileExists(const char* filename) {
    FILE* file = fopen(filename, "r");
    
    if (file != NULL) {
        fclose(file);
        return 1;
    }
    return 0;
}

void createTableFile(const char* tableName, const char* columns) {
    char fileName[1024];
    strcpy(fileName, basePath);
    strcat(fileName, "/");
    strcat(fileName, tableName);
    strcat(fileName, ".txt");

    if (fileExists(fileName)) {
        printf("Table with the same name already exists..\n");
        return;
    }

    FILE* file = fopen(fileName, "w");
    if (file == NULL) {
        printf("Failed to create the table file.\n");
        return;
    }

    char* columnsCopy = strdup(columns);
    char* token = strtok(columnsCopy, "( )\t\n\r");
    while (token != NULL) {
        if (strstr(token, "int") == NULL && strstr(token, "string") == NULL) {
            fprintf(file, "%s\t", token);
        }
            token = strtok(NULL, "( )\t\n\r");
        
    }
    fprintf(file, "\n");

    fclose(file);

    printf("Table file created successfully.\n");
    free(columnsCopy);
}

int deleteDir(const char *parentDir, const char *dirName) {
    char fullPath[1024];
    snprintf(fullPath, sizeof(fullPath), "%s/%s", parentDir, dirName);

    int successFlag = 0; 

    DIR *dir = opendir(parentDir);
    if (dir) {
        struct dirent *entry;
        while ((entry = readdir(dir)) != NULL) {
            if (strcmp(entry->d_name, dirName) == 0) {
                char entryPath[1024];
                snprintf(entryPath, sizeof(entryPath), "%s/%s", parentDir, entry->d_name);

                struct stat entryStat;
                if (stat(entryPath, &entryStat) == 0 && S_ISDIR(entryStat.st_mode)) {
                    deleteDir(entryPath, entry->d_name);
                } else {
                    remove(entryPath);
                }
            }
        }
        closedir(dir);

        if (rmdir(fullPath) == 0) {
            successFlag = 1;
        }
    } else {
        printf("Failed to open the parent directory.\n");
    }

    return successFlag;
}

void insertTable(const char* tableName, const char* columns) {
    char fileName[1024];
    strcpy(fileName, basePath);
    strcat(fileName, "/");
    strcat(fileName, tableName);
    strcat(fileName, ".txt");

    FILE* file = fopen(fileName, "a");
    if (file == NULL) {
        printf("Failed to open the table file.\n");
        return;
    }

    char* columnsCopy = strdup(columns);
    char symbol = ',';
    char col[100];
    strcpy(col, columns);
    char* token = strtok(col, &symbol);

    while (token != NULL) {
        size_t start = strspn(token, " \t\r\n"); 
        size_t length = strcspn(token + start, " \t\r\n");
        char substring[length + 1];

        for (size_t i = 0; i < length; i++) {
            if (isprint(token[start + i])) {
                substring[i] = token[start + i];
            }
        }

        substring[length] = '\0';
        fprintf(file, "%s\t", substring);

        token = strtok(NULL, &symbol);
        
    }
    fprintf(file, "\n");

    fclose(file);

    printf("Table inserted successfully.\n");
    free(columnsCopy);
}

void unuseDb(char* str) {
    char* slashPtr = strchr(str, '/');
    if (slashPtr != NULL) {
        *slashPtr = '\0';
    }
}

void deleteFromTable(const char* tableName) {
    char fileName[1024];
    strcpy(fileName, basePath);
    strcat(fileName, "/");
    strcat(fileName, tableName);
    strcat(fileName, ".txt");

    FILE *file = fopen(fileName, "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return;
    }

    char tempFileName[] = "tempFileFor_Deletion.txt";
    FILE *tempFile = fopen(tempFileName, "w");
    if (tempFile == NULL) {
        printf("Failed to create temporary file.\n");
        fclose(file);
        return;
    }

    char line[100];
    int lineCount = 0;
    int isFirstLine = 1;

    while (fgets(line, sizeof(line), file) != NULL) {
        if (isFirstLine) {
            fputs(line, tempFile);
            isFirstLine = 0;
            continue;
        }

        lineCount++;
    }

    fclose(file);
    fclose(tempFile);

    remove(fileName);
    rename(tempFileName, fileName);

    printf("Successfully deleted %d lines from table %s\n", lineCount, tableName);
}

void deleteFromTableWhere(const char* tableName, const char* column, const char* value) {
    char fileName[1024];
    strcpy(fileName, basePath);
    strcat(fileName, "/");
    strcat(fileName, tableName);
    strcat(fileName, ".txt");

    FILE *file = fopen(fileName, "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return;
    }

    char tempFileName[] = "tempFileFor_DeletionWhere.txt";
    FILE *tempFile = fopen(tempFileName, "w");
    if (tempFile == NULL) {
        printf("Failed to create temporary file.\n");
        fclose(file);
        return;
    }

    char line[1024];
    if (fgets(line, sizeof(line), file) == NULL) {
        printf("File is empty.\n");
        fclose(file);
        fclose(tempFile);
        remove("temp.txt");
        return;
    }
    fputs(line, tempFile);

    char* tmpWord = strtok((char*)line, "\t");
    int pos = 1;
    while (tmpWord != NULL) {
        if (strcmp(tmpWord, column) == 0) break;
        pos++;
        tmpWord = strtok(NULL, "\t");
    }

    int lineCount = 0;
    while (fgets(line, sizeof(line), file) != NULL) {
        char strLine[1024];
        strcpy(strLine, line);

        int wordCount = 0;
        char* word = strtok(line, "\t");
        while (word != NULL) {
            wordCount++;
            if (wordCount == pos) {
                if (strcmp(word, value) == 0) {
                    wordCount = -1;
                    break;
                }
            }
            word = strtok(NULL, "\t");
        }

        if (wordCount == -1) lineCount++;
        else fputs(strLine, tempFile);
    }

    fclose(file);
    fclose(tempFile);

    remove(fileName);
    rename(tempFileName, fileName);

    printf("Successfully deleted %d lines from table %s\n", lineCount, tableName);
}

void updateTable(const char* tableName, const char* column, const char* val) {
    char fileName[1024];
    strcpy(fileName, basePath);
    strcat(fileName, "/");
    strcat(fileName, tableName);
    strcat(fileName, ".txt");

    FILE* file = fopen(fileName, "r");
    if (file == NULL) {
        printf("Failed to open the table file.\n");
        return;
    }

    FILE* tempFile = fopen("temp.txt", "w");
    if (tempFile == NULL) {
        printf("Failed to create temporary file.\n");
        fclose(file);
        return;
    }

    char line[1024];
    if (fgets(line, sizeof(line), file) == NULL) {
        printf("File is empty.\n");
        fclose(file);
        fclose(tempFile);
        remove("temp.txt");  
        return;
    }
    fputs(line, tempFile);

    char* tmpWord = strtok((char*)line, "\t");
    int pos = 1;
    while (tmpWord != NULL) {
        if (strcmp(tmpWord, column) == 0) break;
        pos++;
        tmpWord = strtok(NULL, "\t");
    }

    int updCnt = 0;
    while (fgets(line, sizeof(line), file) != NULL) {
        char* word = strtok(line, "\t");
        int wordCount = 1;

        while (word != NULL) {
            if (wordCount == pos) {
                updCnt++;
                fputs(val, tempFile); 
            } else {
                fputs(word, tempFile); 
            }

            word = strtok(NULL, "\t");
            if (word != NULL) {
                fputc('\t', tempFile);
            }

            wordCount++;
        }
    }

    fclose(file);
    fclose(tempFile);

    remove(fileName);
    rename("temp.txt", fileName);

    printf("Successfully updated %d lines from table %s\n", updCnt, tableName);
}

void updateFromTableWhere(const char* tableName, const char* column, const char* value, const char* wColumn, const char* wValue) {
    char fileName[1024];
    strcpy(fileName, basePath);
    strcat(fileName, "/");
    strcat(fileName, tableName);
    strcat(fileName, ".txt");

    FILE *file = fopen(fileName, "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return;
    }

    char tempFileName[] = "tempFileFor_UpdateWhere.txt";
    FILE *tempFile = fopen(tempFileName, "w");
    if (tempFile == NULL) {
        printf("Failed to create temporary file.\n");
        fclose(file);
        return;
    }

    char line[1024];
    if (fgets(line, sizeof(line), file) == NULL) {
        printf("File is empty.\n");
        fclose(file);
        fclose(tempFile);
        remove("temp.txt");  
        return;
    }
    fputs(line, tempFile);

    int pos, wPos;

    char* tmpWord = strtok((char*)line, "\t");
    int i = 1;
    while (tmpWord != NULL) {
        if (strcmp(tmpWord, column) == 0) pos = i;
        if (strcmp(tmpWord, wColumn) == 0) wPos = i;
        i++;
        tmpWord = strtok(NULL, "\t");
    }

    int updCnt = 0;
    while (fgets(line, sizeof(line), file) != NULL) {
        char strLine[1024];
        strcpy(strLine, line);

        int wordCount = 0;
        char* word1 = strtok(line, "\t");
        while (word1 != NULL) {
            wordCount++;
            if (wordCount == wPos) {
                if (strcmp(word1, wValue) == 0) {
                    wordCount = -1;
                    break;
                }
            }
            word1 = strtok(NULL, "\t");
        }

        if (wordCount == -1) {
            char* word = strtok(strLine, "\t");
            wordCount = 1;

            while (word != NULL) {
                if (wordCount == pos) {
                    updCnt++;
                    fputs(value, tempFile); 
                } else {
                    fputs(word, tempFile); 
                }

                word = strtok(NULL, "\t");
                if (word != NULL) {
                    fputc('\t', tempFile); 
                }

                wordCount++;
            }
        }
        else fputs(strLine, tempFile);
    }

    fclose(file);
    fclose(tempFile);

    remove(fileName);
    rename(tempFileName, fileName);

    printf("Successfully updated %d lines from table %s\n", updCnt, tableName);
}

void selectFromTable(const char* tableName, char* columns) {
    char fileName[1024];
    strcpy(fileName, basePath);
    strcat(fileName, "/");
    strcat(fileName, tableName);
    strcat(fileName, ".txt");

    FILE* file = fopen(fileName, "r");
    if (file == NULL) {
        printf("Failed to open the table file.\n");
        return;
    }

    char line[1024];
    if (fgets(line, sizeof(line), file) == NULL) {
        printf("File is empty.\n");
        fclose(file);
        return;
    }

    size_t length = strlen(columns);
    if (length > 0 && columns[length - 1] == ' ') {
        columns[length - 1] = '\0';
    }

    if (strcmp(columns, "*") == 0) {
        while (fgets(line, sizeof(line), file) != NULL) printf("%s", line);
    } else {
        int pos = 1;
        if (strchr(columns, ',') != NULL) {
            char* colName[100];
            int colIdx = 0;

            char* col = strtok(columns, ", ");
            while (col != NULL && colIdx < 100) {
                colName[colIdx++] = col;
                col = strtok(NULL, ", ");
            }

            int colPos[100];
            for (int i = 0; i < colIdx; i++) {
                colPos[i] = -1;
            }

            char* word = strtok((char*)line, "\t");
            int pos = 1;
            while (word != NULL) {
                for (int i = 0; i < colIdx; i++) {
                    if (strcmp(word, colName[i]) == 0) {
                        colPos[i] = pos;
                        break;
                    }
                }
                pos++;
                word = strtok(NULL, "\t");
            }

            while (fgets(line, sizeof(line), file) != NULL) {
                char* colVal[100];
                int valIdx = 0;
                char* val = strtok(line, "\t");

                while (val != NULL) {
                    colVal[valIdx++] = val;
                    val = strtok(NULL, "\t");
                }

                for (int i = 0; i < colIdx; i++) {
                    printf("%s\t", colVal[colPos[i] - 1]);
                }
                printf("\n");
            }
        } else {
            char* tmpWord = strtok((char*)line, "\t");
            while (tmpWord != NULL) {
                if (strcmp(tmpWord, columns) == 0) break;
                pos++;
                tmpWord = strtok(NULL, "\t");
            }

            while (fgets(line, sizeof(line), file) != NULL) {
                char* word = strtok(line, "\t");
                int wordCount = 1;

                while (word != NULL) {
                    if (wordCount == pos) {
                        printf("%s\t", word);
                        break;
                    }

                    word = strtok(NULL, "\t");
                    wordCount++;
                }

                printf("\n");
            }
        }
    }

    fclose(file);
}


void signalHandler(int signal) {
    if (signal == SIGCHLD) {
        pid_t chPID;
        int status;
        while ((chPID = waitpid(-1, &status, WNOHANG)) > 0) {
            printf("User %s has disconnected from server.\n", uName);
        }
    }
}

void selectFromTableWhere(const char* tableName, char* columns, const char* wColumn, const char* wValue) {
    char fileName[1024];
    strcpy(fileName, basePath);
    strcat(fileName, "/");
    strcat(fileName, tableName);
    strcat(fileName, ".txt");

    FILE* file = fopen(fileName, "r");
    if (file == NULL) {
        printf("Failed to open the table file.\n");
        return;
    }

    char line[1024], strLine[1024];
    if (fgets(line, sizeof(line), file) == NULL) {
        printf("File is empty.\n");
        fclose(file);
        return;
    }

    strcpy(strLine, line);

    char* tmpWord = strtok((char*)line, "\t");
    int wPos = 1;
    while (tmpWord != NULL) {
        if (strcmp(tmpWord, wColumn) == 0) break;
        wPos++;
        tmpWord = strtok(NULL, "\t");
    }

    size_t length = strlen(columns);
    if (length > 0 && columns[length - 1] == ' ') {
        columns[length - 1] = '\0';
    }

    if (strcmp(columns, "*") == 0) {
        while (fgets(line, sizeof(line), file) != NULL) {
            strcpy(strLine, line);

            int wordCount = 0;
            char* word = strtok((char*)line, "\t");
            while (word != NULL) {
                wordCount++;
                if (wordCount == wPos) {
                    if (strcmp(word, wValue) == 0) {
                        wordCount = -1;
                        break;
                    }
                }
                word = strtok(NULL, "\t");
            }

            if (wordCount == -1) printf("%s", strLine);
        }
    } else {
        int pos = 1;
        if (strchr(columns, ',') != NULL) {
            char* colName[100];
            int colIdx = 0;

            char* col = strtok(columns, ", ");
            while (col != NULL && colIdx < 100) {
                colName[colIdx++] = col;
                col = strtok(NULL, ", ");
            }

            int colPos[100];
            for (int i = 0; i < colIdx; i++) {
                colPos[i] = -1;
            }

            char* word1 = strtok((char*)strLine, "\t");
            pos = 1;
            while (word1 != NULL) {
                for (int i = 0; i < colIdx; i++) {
                    if (strcmp(word1, colName[i]) == 0) {
                        colPos[i] = pos;
                        break;
                    }
                }
                pos++;
                word1 = strtok(NULL, "\t");
            }

            while (fgets(line, sizeof(line), file) != NULL) {
                strcpy(strLine, line);

                int wordCount = 0;
                char* word = strtok((char*)strLine, "\t");
                while (word != NULL) {
                    wordCount++;
                    if (wordCount == wPos) {
                        if (strcmp(word, wValue) == 0) {
                            wordCount = -1;
                            break;
                        }
                    }
                    word = strtok(NULL, "\t");
                }

                if (wordCount == -1) {
                    char* colVal[100];
                    int valIdx = 0;
                    char* val = strtok((char*)line, "\t");

                    while (val != NULL) {
                        colVal[valIdx++] = val;
                        val = strtok(NULL, "\t");
                    }

                    for (int i = 0; i < colIdx; i++) {
                        printf("%s\t", colVal[colPos[i] - 1]);
                    }
                    printf("\n");
                }
            }
        } else {
            char* tmpWord = strtok((char*)strLine, "\t");
            while (tmpWord != NULL) {
                if (strcmp(tmpWord, columns) == 0) break;
                pos++;
                tmpWord = strtok(NULL, "\t");
            }

            while (fgets(line, sizeof(line), file) != NULL) {
                strcpy(strLine, line);

                int wordCount = 0;
                char* word1 = strtok((char*)line, "\t");
                while (word1 != NULL) {
                    wordCount++;
                    if (wordCount == wPos) {
                        if (strcmp(word1, wValue) == 0) {
                            wordCount = -1;
                            break;
                        }
                    }
                    word1 = strtok(NULL, "\t");
                }

                if (wordCount == -1) {
                    char* word = strtok((char*)strLine, "\t");
                    int wordCount = 1;

                    while (word != NULL) {
                        if (wordCount == pos) {
                            printf("%s\t", word);
                            break;
                        }

                        word = strtok(NULL, "\t");
                        wordCount++;
                    }

                    printf("\n");
                }
            }
        }
    }

    fclose(file);
}

void dropColumnFromTable(const char* tableName, const char* column) {
    char fileName[1024];
    strcpy(fileName, basePath);
    strcat(fileName, "/");
    strcat(fileName, tableName);
    strcat(fileName, ".txt");

    FILE *file = fopen(fileName, "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return;
    }

    char tempFileName[] = "tempFileFor_DropCol.txt";
    FILE *tempFile = fopen(tempFileName, "w");
    if (tempFile == NULL) {
        printf("Failed to create temporary file.\n");
        fclose(file);
        return;
    }

    char line[1024], strLine[1024];
    if (fgets(line, sizeof(line), file) == NULL) {
        printf("File is empty.\n");
        fclose(file);
        fclose(tempFile);
        remove("temp.txt");  
        return;
    }
    strcpy(strLine, line);

    int pos = -1;
    char* tmpWord = strtok((char*)line, "\t");
    int i = 1;
    while (tmpWord != NULL) {
        if (strcmp(tmpWord, column) == 0) pos = i;
        i++;
        tmpWord = strtok(NULL, "\t");
    }

    if (pos == -1) {
        printf("Column %s not found in table %s.\n", column, tableName);
        return;
    }

    int wordCount = 1;
    char* word = strtok((char*)strLine, "\t");
    while (word != NULL) {
        if (wordCount != pos) {
            fputs(word, tempFile); 
            word = strtok(NULL, "\t");
            if (word != NULL) {
                fputc('\t', tempFile); 
            }
        } else {
            word = strtok(NULL, "\t");
        }

        wordCount++;
    }

    while (fgets(line, sizeof(line), file) != NULL) {
        strcpy(strLine, line);

        wordCount = 1;
        char* word1 = strtok((char*)strLine, "\t");
        while (word1 != NULL) {
            if (wordCount != pos) {
                fputs(word1, tempFile); 
                word1 = strtok(NULL, "\t");
                if (word1 != NULL) {
                    fputc('\t', tempFile); 
                }
            } else {
                word1 = strtok(NULL, "\t");
            }

            wordCount++;
        }
    }

    fclose(file);
    fclose(tempFile);

    remove(fileName);
    rename(tempFileName, fileName);

    printf("Successfully dropped column %s from table %s\n", column, tableName);
}

void append_permission(const char *uname, const char *permission) {
    FILE *file, *temp;
    char line[1024];

    file = fopen(FILENAME, "r");
    temp = fopen("temp.txt", "w");

    if (file == NULL) {
        printf("Error opening user_list.txt: %s\n", strerror(errno));
        return;
    }

    if (temp == NULL) {
        printf("Error opening temp.txt: %s\n", strerror(errno));
        return;
    }

    if (file == NULL || temp == NULL) {
        perror("Error opening files.");
        return;
    }

    while (fgets(line, sizeof(line), file)) {       
        line[strcspn(line, "\n")] = '\0';

        char *pos = strstr(line, uname);

        if (pos != NULL) {
            fprintf(temp, "%s - %s\n", line, permission);
        } else {
            fputs(line, temp);
            fputc('\n', temp); 
        }
    }

    fclose(file);
    fclose(temp);

    if (rename("temp.txt", FILENAME) != 0) {
        perror("Error replacing file.");
        return;
    }

    printf("Permission appended successfully.\n");
}

void handleClient(int clSocket) {
    char buffer[1024];
    int rdBytes;
    char use_database[100] = "";

    while (1) {
        rdBytes = recv(clSocket, buffer, sizeof(buffer), 0);
        if (rdBytes <= 0) {
            break;
        }
        buffer[rdBytes - 1] = '\0';
        printf("\nReceived: %s\n", buffer);

        char* createDbCmd = "CREATE DATABASE";
        char* createUserCmd = "CREATE USER";
        char* grantPermCmd = "GRANT PERMISSION";
        char* dropDbCmd = "DROP DATABASE";
        char* useDbCmd = "USE";
        char* createTableCmd = "CREATE TABLE";
        char* dropTableCmd = "DROP TABLE";
        char* dropColumnCmd = "DROP COLUMN";
        char* insertCmd = "INSERT INTO";
        char* deleteCmd = "DELETE FROM";
        char* updateCmd = "UPDATE";
        char* selectCmd = "SELECT";
        char* whereCmd = " WHERE ";

        if (strstr(buffer, createDbCmd) != NULL) {
            char* dbNameStart = buffer + strlen(createDbCmd);
            while (*dbNameStart == ' ') {
                dbNameStart++;
            }
                
            char folderPath[100];
            sprintf(folderPath, "databases/%s", dbNameStart);

            if (mkdir("databases", 0777) == 0) {
                printf("Created 'databases' folder: %s\n", folderPath);
            } 

            if (mkdir(folderPath, 0777) == 0) {
                printf("Created folder: %s\n", folderPath);
            } else {
                printf("Failed to create database folder: %s\n", folderPath);
            }
            write_log(uName, buffer);
        }
        
        else if (strstr(buffer, createUserCmd) != NULL) {
            char* unameStart = buffer + strlen(createUserCmd);
            while (*unameStart == ' ') {
                unameStart++;
            }
            char* passwordStart = strstr(unameStart, "IDENTIFIED BY");
            if (passwordStart != NULL) {
                *passwordStart = '\0';
                passwordStart += strlen("IDENTIFIED BY");

                char* uname = strtok(unameStart, " ");
                char* password = strtok(passwordStart, " ");
                char folderPath[] = "databases/users";
                char filename[] = "databases/users/user_list.txt";  

                if (mkdir("databases", 0777) == 0) {
                    printf("Created 'databases' folder\n");
                }

                if (mkdir(folderPath, 0777) == 0) {
                    printf("Created 'users' folder\n");
                }

                FILE* file = fopen(filename, "a");
                if (file != NULL) {
                    fseek(file, 0, SEEK_END);
                    long fileSize = ftell(file);
                    if (fileSize == 0) {
                        fprintf(file, "uname - PASSWORD\n");  
                    }
                    fprintf(file, "%s - %s\n", uname, password);  
                    fclose(file);
                    printf("Created user: %s\n", uname);
                } else {
                    printf("Failed to create user: %s\n", uname);
                }

            }
            write_log(uName, buffer);
        }

        else if (strstr(buffer, grantPermCmd) != NULL){
            char dbName[100];
            char uname[500];
            sscanf(buffer, "GRANT PERMISSION %[^ ] INTO %s", dbName, uname);
            append_permission(uname, "access");
            write_log(uName, buffer);
        }

        else if (strstr(buffer, dropDbCmd) != NULL) {
            int check, access = 0;

            char* dbTarget = buffer + strlen(dropDbCmd);
            while (*dbTarget == ' ') {
                dbTarget++;
            }

            if (dbTarget == NULL) {
                printf("Invalid input\n");
                send(clSocket, buffer, strlen(buffer), 0);
                memset(buffer, 0, sizeof(buffer));
                continue;
            }

            access = 1;

            switch (access) {
                case 1:                    
                    if (deleteDir("databases", dbTarget)) {
                        printf("Successfully dropped database %s\n", dbTarget);
                    } else {
                        printf("Unable to find database %s\n", dbTarget);
                    }
                    break;

                case 2:
                    printf("User unable to drop database %s\n", dbTarget);
                    break;
            }
            write_log(uName, buffer);
        }

        else if (strstr(buffer, useDbCmd) != NULL) {
            char* dbTarget = buffer + strlen(useDbCmd);

            int check = check_access(uName);

            if(check || !strcmp(uName, "root")){
                while (*dbTarget == ' ') {
                    dbTarget++;
                }

                char dirName[1024];
                strcpy(dirName, basePath);
                strcat(dirName, "/");
                strcat(dirName, dbTarget);

                if (dbTarget == NULL || !directoryExists(dirName)) 
                {
                    printf("Unable to find database %s\n", dbTarget);
                    send(clSocket, buffer, strlen(buffer), 0);
                    memset(buffer, 0, sizeof(buffer));
                    continue;
                }

                unuseDb(basePath);

                strcat(basePath, "/");
                strcat(basePath, dbTarget);

                printf("Successfully used database %s\n", dbTarget);
                write_log(uName, buffer);
            }
            else {
                if (check == -1) {
                    printf("There are no users other than root.\n");
                    send(clSocket, buffer, strlen(buffer), 0);
                    memset(buffer, 0, sizeof(buffer));
                    continue;
                }
                printf("User %s doesn't have access to database %s\n", uName, dbTarget);
            }

        }

        else if (strstr(buffer, dropTableCmd) != NULL) {
            if (strchr(basePath, '/') == NULL) {
                printf("Please USE a database first!\n");
                send(clSocket, buffer, strlen(buffer), 0);
                memset(buffer, 0, sizeof(buffer));
                continue;
            }

            char* tableName = buffer + strlen(createUserCmd);
            while (*tableName == ' ') {
                tableName++;
            }

            char delPath[1024];
            strcpy(delPath, basePath);
            strcat(delPath, "/");
            strcat(delPath, tableName);
            strcat(delPath, ".txt");

            FILE* file = fopen(delPath, "r");
            if (file != NULL) {
                fclose(file);
                if (remove(delPath) == 0) {
                    printf("Table %s deleted successfully.\n", tableName);
                } else {
                    printf("Unable to delete %s table.\n", tableName);
                }
            } else printf("Table %s not found in %s.\n", tableName, basePath);
            write_log(uName, buffer);
        }

        else if (strstr(buffer, dropColumnCmd) != NULL) {
            if (strchr(basePath, '/') == NULL) {
                printf("Please USE a database first!\n");
                send(clSocket, buffer, strlen(buffer), 0);
                memset(buffer, 0, sizeof(buffer));
                continue;
            }

            char tableName[100];
            char column[500];
            sscanf(buffer, "DROP COLUMN %[^ ] FROM %s", column, tableName);
            dropColumnFromTable(tableName, column);
            write_log(uName, buffer);
        }

        else if (strstr(buffer, createTableCmd) != NULL) {
            if (strchr(basePath, '/') == NULL) {
                printf("Please USE a database first!\n");
                send(clSocket, buffer, strlen(buffer), 0);
                memset(buffer, 0, sizeof(buffer));
                continue;
            }

            char tableName[100];
            char columns[500];
            sscanf(buffer, "CREATE TABLE %[^ ] (%[^)]", tableName, columns);

            createTableFile(tableName, columns);
            write_log(uName, buffer);
        }

        else if (strstr(buffer, insertCmd) != NULL) {
            if (strchr(basePath, '/') == NULL) {
                printf("Please USE a database first!\n");
                send(clSocket, buffer, strlen(buffer), 0);
                memset(buffer, 0, sizeof(buffer));
                continue;
            }

            char tableName[100];
            char columns[500];
            sscanf(buffer, "INSERT INTO %[^ ] (%[^)]", tableName, columns);

            insertTable(tableName, columns);
            write_log(uName, buffer);
        }

        else if (strstr(buffer, deleteCmd) != NULL && strstr(buffer, whereCmd) != NULL) {
            if (strchr(basePath, '/') == NULL) {
                printf("Please USE a database first!\n");
                send(clSocket, buffer, strlen(buffer), 0);
                memset(buffer, 0, sizeof(buffer));
                continue;
            }

            char tableName[100];
            char column[500];
            char value[100];
            sscanf(buffer, "DELETE FROM %[^ ] WHERE %[^=]=%s", tableName, column, value);
            deleteFromTableWhere(tableName, column, value);
            write_log(uName, buffer);
        }

        else if (strstr(buffer, deleteCmd) != NULL) {
            if (strchr(basePath, '/') == NULL) {
                printf("Please USE a database first!\n");
                send(clSocket, buffer, strlen(buffer), 0);
                memset(buffer, 0, sizeof(buffer));
                continue;
            }

            char* tableTarget = buffer + strlen(deleteCmd);
            while (*tableTarget == ' ') {
                tableTarget++;
            }

            deleteFromTable(tableTarget);
            write_log(uName, buffer);
        }

        else if (strstr(buffer, updateCmd) != NULL && strstr(buffer, whereCmd) != NULL) {
            if (strchr(basePath, '/') == NULL) {
                printf("Please USE a database first!\n");
                send(clSocket, buffer, strlen(buffer), 0);
                memset(buffer, 0, sizeof(buffer));
                continue;
            }

            char tableName[100];
            char column[500];
            char value[100];
            char wColumn[500];
            char wValue[100];
            sscanf(buffer, "UPDATE %[^ ] SET %[^=]=%[^ ] WHERE %[^=]=%s", tableName, column, value, wColumn, wValue);
            updateFromTableWhere(tableName, column, value, wColumn, wValue);
            write_log(uName, buffer);
        }

        else if (strstr(buffer, updateCmd) != NULL) {
            if (strchr(basePath, '/') == NULL) {
                printf("Please USE a database first!\n");
                send(clSocket, buffer, strlen(buffer), 0);
                memset(buffer, 0, sizeof(buffer));
                continue;
            }

            char tableName[100];
            char columns[500];
            char value[100];
            sscanf(buffer, "UPDATE %[^ ] SET %[^=]=%s", tableName, columns, value);
            updateTable(tableName, columns, value);
            write_log(uName, buffer);
        }

        else if (strstr(buffer, selectCmd) != NULL && strstr(buffer, whereCmd) != NULL) {
            if (strchr(basePath, '/') == NULL) {
                printf("Please USE a database first!\n");
                send(clSocket, buffer, strlen(buffer), 0);
                memset(buffer, 0, sizeof(buffer));
                continue;
            }

            char tableName[100];
            char columns[500];
            char wColumn[500];
            char wValue[100];
            sscanf(buffer, "SELECT %[^FROM] FROM %[^ ] WHERE %[^=]=%s", columns, tableName, wColumn, wValue);
            selectFromTableWhere(tableName, columns, wColumn, wValue);
            write_log(uName, buffer);
        }

        else if (strstr(buffer, selectCmd) != NULL) {
            if (strchr(basePath, '/') == NULL) {
                printf("Please USE a database first!\n");
                send(clSocket, buffer, strlen(buffer), 0);
                memset(buffer, 0, sizeof(buffer));
                continue;
            }

            char tableName[100];
            char columns[500];
            sscanf(buffer, "SELECT %[^FROM] FROM %s", columns, tableName);

            selectFromTable(tableName, columns);
            write_log(uName, buffer);
        }

        else printf("Command is not recognized..\n");

        send(clSocket, buffer, strlen(buffer), 0);
        memset(buffer, 0, sizeof(buffer));
    }

    close(clSocket);
}

int main() {
    int srvSocket, clSocket;
    struct sockaddr_in srvAddress, clAddress;
    socklen_t addrSize;
    pid_t chPID;

    signal(SIGCHLD, signalHandler);

    srvSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (srvSocket == -1) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    srvAddress.sin_family = AF_INET;
    srvAddress.sin_addr.s_addr = INADDR_ANY;
    srvAddress.sin_port = htons(PORT);

    if (bind(srvSocket, (struct sockaddr*)&srvAddress, sizeof(srvAddress)) == -1) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(srvSocket, MAX_CLIENTS) == -1) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

    printf("Server listening on port %d.\n", PORT);

    while (1) {
        addrSize = sizeof(clAddress);
        clSocket = accept(srvSocket, (struct sockaddr*)&clAddress, &addrSize);
        if (clSocket == -1) {
            perror("Accept failed");
            continue;
        }

        recv(clSocket, uName, sizeof(uName), 0);
        printf("User %s connected to server.\n", uName);

        chPID = fork();
        if (chPID == 0) {
            close(srvSocket);
            handleClient(clSocket);
            exit(EXIT_SUCCESS);
        } else if (chPID > 0) {
            close(clSocket);
        } else {
            perror("Fork failed");
            exit(EXIT_FAILURE);
        }
    }

    close(srvSocket);
    return 0;
}
