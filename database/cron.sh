#!/bin/bash

cd /fpsisop/database

/usr/bin/zip -rm /fpsisop/dump/$(date "+%Y-%m-%d")\ $(date +"%H:%M:%S") databases/log.txt /fpsisop/dump/*_backup.txt
