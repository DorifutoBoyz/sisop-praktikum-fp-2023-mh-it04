# sisop-praktikum-fp-2023-MH-IT04

| Nama | NRP | Kontribusi|
| ------ | ------ | ----- |
|Marselinus Krisnawan Riandika|5027221056|IYA|
|Aras Rizky Ananta|5027221053|TIDAK|
|Atha Rahma Arianti|5027221030|IYA|

## Laporan Resmi Final Project Praktikum Sistem Operasi 2023




1. **Penjelasan Kode Client.c**

   **Header** 


        #include <stdio.h>
        #include <stdlib.h>
        #include <string.h>
        #include <unistd.h>
        #include <sys/socket.h>
        #include <netinet/in.h>
        #include <arpa/inet.h>

    Berikut merupakan beberapa header yang kami gunakan untuk client.c

    **Konstanta**

        #define SERVER_IP "127.0.0.1"
        #define PORT 8080
        #define textCmd_SIZE 1024

    Kode ini mendefinisikan alamat IP server, nomor port, dan ukuran maksimal pesan yang dapat dikirim/diterima.

    **Program Utama**
       
        if (sudo_user == NULL) {

    Kode ini digunakan untuk mengecek program dijalankan secara root atau tidak

         isRoot = 0;

    Jika program tidak dijalankan sebagai root, variabel isRoot diatur menjadi 0, menandakan bahwa program berjalan dalam mode pengguna biasa.

        if (argc != 5) {
         printf("Invalid number of arguments.\n");
         return 1;
        }

    Program memeriksa apakah jumlah argumen baris perintah sesuai dengan yang diharapkan. Jika tidak, program mencetak pesan kesalahan dan keluar.

        char *uPass = NULL;

	    for (int i = 1; i < argc; i++) {
	    if (strcmp(argv[i], "-u") == 0 && i + 1 < argc) {
        uName = argv[i + 1];
	    i++;  
	    } else if (strcmp(argv[i], "-p") == 0 && i + 1 < argc) {
        uPass = argv[i + 1];
        i++; 
        }
	    }

    Program melakukan iterasi melalui argumen baris perintah untuk menemukan opsi -u dan -p yang menyatakan nama pengguna dan kata sandi. Informasi ini disimpan dalam variabel uName dan uPass.

        if (uName == NULL || uPass == NULL) {
            printf("Invalid arguments.\n");
            return 1;
        }

    Program memeriksa apakah kedua opsi -u dan -p telah ditemukan dan diatur. Jika tidak, program mencetak pesan kesalahan dan keluar

        FILE *file = fopen("../database/databases/users/user_list.txt", "r");
        if (file != NULL) {

    Program mencoba membuka file yang berisi daftar pengguna dan kata sandi. Jika file dapat dibuka, program melanjutkan untuk membaca isinya.

        char line[256];
        fgets(line, sizeof(line), file); 
        int isAuthorized = 0;

        while (fgets(line, sizeof(line), file) != NULL) {
        // ...
        }

    Program membaca baris pertama dari file (yang mungkin berisi header) dan kemudian melakukan iterasi melalui setiap baris selanjutnya. Setiap baris diproses untuk mendapatkan username dan password, dan kemudian membandingkannya dengan input pengguna.

        fclose(file);

        if (!isAuthorized) {
         printf("Invalid username or password.\n");
        return 1;
        }

    Setelah membaca seluruh file, program menutup file dan memeriksa apakah autentikasi berhasil. Jika tidak, mencetak pesan kesalahan dan keluar.

        } else {
        printf("User list file not found.\n");
        return 1;
        }

    Jika file pengguna tidak dapat dibuka, program mencetak pesan kesalahan dan keluar.

        } else {
        uName = "root";
        }

    Jika program dijalankan sebagai root, variabel uName diatur menjadi "root" sebagai pengguna default.

        int clSocket;
        struct sockaddr_in srvAddr;
        char textCmd[textCmd_SIZE];

    Membuat variabel untuk socket client (clSocket), struktur alamat server (srvAddr), dan buffer untuk pesan (textCmd)

        clSocket = socket(AF_INET, SOCK_STREAM, 0);
        if (clSocket == -1) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
        }

    Membuat socket dengan menggunakan protokol IPv4 (AF_INET) dan tipe socket SOCK_STREAM untuk koneksi TCP. Jika pembuatan socket gagal, program mencetak pesan kesalahan dan keluar

        srvAddr.sin_family = AF_INET;
        srvAddr.sin_port = htons(PORT);
        if (inet_pton(AF_INET, SERVER_IP, &(srvAddr.sin_addr)) <= 0) {
            perror("Invalid address/Address not supported");
            exit(EXIT_FAILURE);
        }

    Mengatur famili alamat dan nomor port server dalam bentuk yang sesuai untuk digunakan dalam koneksi socket. Fungsi inet_pton mengonversi alamat IP dari string ke representasi biner. Jika konversi gagal, program mencetak pesan kesalahan dan keluar.

        if (connect(clSocket, (struct sockaddr*)&srvAddr, sizeof(srvAddr)) == -1) {
            perror("Connection failed");
            exit(EXIT_FAILURE);
        }

    Mencoba menghubungkan socket ke server menggunakan alamat yang telah diinisialisasi sebelumnya. Jika koneksi gagal, program mencetak pesan kesalahan dan keluar.

        send(clSocket, uName, strlen(uName), 0);

    Mengirimkan nama pengguna ke server menggunakan fungsi send. Panjang nama pengguna dihitung menggunakan strlen.

        while (1) {
            printf("Enter a message (or 'q' to quit): ");
            fgets(textCmd, sizeof(textCmd), stdin);

            send(clSocket, textCmd, strlen(textCmd), 0);

            if (textCmd[0] == 'q')
                break;

            memset(textCmd, 0, sizeof(textCmd));
            recv(clSocket, textCmd, sizeof(textCmd), 0);
        }

    Program memasuki loop tak terbatas yang meminta pengguna untuk memasukkan pesan dari keyboard. Pesan tersebut dikirimkan ke server menggunakan fungsi send. Jika pengguna memasukkan 'q', loop berakhir.

    Setelah mengirim pesan, program menggunakan recv untuk menerima respons dari server dan menampilkannya di layar. Buffer textCmd diatur ulang menggunakan memset untuk persiapan menerima pesan selanjutnya.

        close(clSocket);
        return 0;

    Setelah loop selesai, socket ditutup dan program keluar. Ini akan membebaskan sumber daya dan menyelesaikan eksekusi program.

2. **Penjelasan Kode client_dump.c**

    **Header**

        #include <stdio.h>
        #include <stdlib.h>
        #include <string.h>
        #include <unistd.h>
        #include <sys/socket.h>
        #include <sys/types.h>
        #include <netinet/in.h>
        #include <arpa/inet.h>
        #include <sys/stat.h>
        #include <dirent.h>
        #include <ctype.h>
        #include <time.h>

    Baris ini merupakan inklusi dari beberapa header file yang menyediakan deklarasi fungsi dan konstanta yang diperlukan untuk berbagai operasi dalam program.

    **Variable Global**

        char *uPass = NULL;
        char *uName = NULL;

    Variabel global untuk menyimpan nama pengguna dan kata sandi, yang kemungkinan akan digunakan untuk autentikasi.

    **Fungsi Utama**

        int isRoot = 1;
        char *sudo_user = getenv("SUDO_USER");

        if (sudo_user == NULL) {
            // ...
        } else {
            uName = "root";
        }

    Program mengecek apakah dijalankan sebagai root atau tidak. Jika tidak, maka program memproses argumen baris perintah untuk mendapatkan nama pengguna dan kata sandi, dan kemudian memeriksa keberadaan dan validitas pengguna di file user_list.txt. Jika dijalankan sebagai root, nama pengguna diatur sebagai "root".

        FILE *fp, *dp;
        fp = fopen("../database/log.txt", "r");
        dp = fopen("command_backup.txt", "w");
        // ...

    Program membuka file log (log.txt) untuk dibaca dan file command_backup.txt untuk ditulis. Jika file log tidak dapat dibuka, program mencetak pesan kesalahan dan keluar.

        char buffer[20000];
        while (fgets(buffer, sizeof(buffer), fp)) {
            // ...
        }

    Program menggunakan loop untuk membaca baris-baris dari file log. Setiap baris diproses untuk mendapatkan informasi yang relevan, dan data tersebut ditulis ke file command_backup.txt.

        fclose(fp);
        fclose(dp);

    Setelah selesai membaca dan menulis, program menutup file-file yang telah dibuka.

3. **Penjelasan Kode Cron.sh**

        cd /fpsisop/database

    Baris ini berpindah ke direktori /fpsisop/database.

        /usr/bin/zip -rm /fpsisop/dump/$(date "+%Y-%m-%d")\$(date +"%H:%M:%S") databases/log.txt /fpsisop/dump/*_backup.txt

- /usr/bin/zip: Ini adalah perintah untuk membuat dan mengelola arsip zip.
- rm: Menghapus file dari arsip setelah ditambahkan ke arsip.
- /fpsisop/dump/$(date "+%Y-%m-%d")\$(date +"%H:%M:%S"): Ini adalah jalur dan nama file untuk arsip zip yang dibuat. Nama file ini dihasilkan dengan menggabungkan tanggal (format Y-m-d) dan waktu (format H:M:S) saat ini.
- databases/log.txt: File log.txt dari direktori databases akan dimasukkan ke dalam arsip.
- /fpsisop/dump/*_backup.txt: Semua file yang sesuai dengan pola *_backup.txt dari direktori dump juga dimasukkan ke dalam arsip.

4. **Penjelasan Kode Database.c**

    **Header**

        #include <stdio.h>
        #include <stdlib.h>
        #include <string.h>
        #include <unistd.h>
        #include <sys/socket.h>
        #include <netinet/in.h>
        #include <arpa/inet.h>
        #include <sys/types.h>
        #include <sys/wait.h>
        #include <sys/stat.h>
        #include <fcntl.h>
        #include <signal.h>
        #include <dirent.h>
        #include <ctype.h>
        #include <time.h>
        #include <errno.h>

    Berikut adalah header yang digunakan dalam kode ini

    **Konstanta dan Variabel**

        #define PORT 8080
        #define MAX_CLIENTS 5
        #define FILENAME "databases/users/user_list.txt"

    Konstanta ini menyediakan nilai tetap untuk nomor port server, jumlah maksimum klien yang dapat dilayani, dan nama file yang digunakan untuk menyimpan daftar pengguna.

    **Cek Akses**
    
        int check_access(const char *uname)

    Fungsi ini membuka file dengan nama yang diacu oleh FILENAME dan mencari apakah pengguna dengan nama yang diberikan memiliki akses. Jika pengguna memiliki akses (ditemukan kata "access" di baris yang sesuai), fungsi mengembalikan 1; jika tidak, mengembalikan 0. Jika gagal membuka file, mengembalikan -1.

    **Tulis Log**

        void write_log(const char *uname, const char *command)

    Fungsi ini menulis entri log dengan format timestamp, nama pengguna (uname), dan perintah (command) ke file "log.txt". Jika file log tidak dapat dibuka untuk ditambahkan ("a"), fungsi tidak melakukan apa-apa.

    **Directory Exist** 

        int directoryExists(const char* dirPth)

    Fungsi ini memeriksa keberadaan direktori dengan path yang diberikan. Jika direktori tersebut ada, mengembalikan 1; jika tidak, mengembalikan 0.

    **File Exist**

        int fileExists(const char* filename)

    Fungsi ini memeriksa keberadaan file dengan nama yang diberikan. Jika file tersebut ada, mengembalikan 1; jika tidak, mengembalikan 0.

    **Create Table File**

        void createTableFile(const char* tableName, const char* columns)

    Fungsi ini membuat file untuk tabel dengan nama tableName dan kolom yang diberikan (columns). Jika file dengan nama yang sama sudah ada, mencetak pesan kesalahan. Fungsi ini menulis nama kolom ke file baru dengan format tertentu. Kolom-kolom yang diizinkan adalah yang memiliki tipe "int" atau "string". Setelah menulis kolom, file ditutup.

    **Delete Dir**

        int deleteDir(const char *parentDir, const char *dirName)

    Fungsi ini menghapus direktori rekursif bersama dengan semua file dan subdirektori di dalamnya. Pertama, dibuat jalur lengkap (fullPath) menggunakan snprintf. Kemudian, direktori parentDir dibuka dan setiap entri di dalamnya diperiksa. Jika entri merupakan direktori, fungsi deleteDir dipanggil rekursif untuk menghapusnya. Jika entri bukan direktori, file tersebut dihapus dengan remove. Setelah itu, direktori parentDir dihapus dengan rmdir. Fungsi mengembalikan 1 jika penghapusan berhasil, dan 0 jika gagal.

    **insert Table**

        void insertTable(const char* tableName, const char* columns)

    Fungsi ini membuka file tabel untuk ditambahkan ("a") dan menulis kolom yang diberikan ke dalamnya. Kolom dipisahkan oleh koma (,), dan setiap nilai kolom dipisahkan oleh tab (\t). Fungsi mengembalikan pesan keberhasilan atau kegagalan.

    **unuse Database**

        void unuseDb(char* str)

    Fungsi ini memotong string str pada karakter '/' pertama yang ditemukan. Jika tidak ada karakter '/', tidak ada yang diubah. Fungsi ini dapat digunakan untuk menghapus bagian dari path yang terletak setelah karakter '/'.

    **delete From Table**

        void deleteFromTable(const char* tableName)

    Fungsi ini menghapus semua baris (record) dari tabel dengan nama tableName. Pertama, dibuka file tabel untuk dibaca ("r"), dan sebuah file sementara (tempFileFor_Deletion.txt) dibuka untuk ditulis ("w"). Sebuah loop membaca baris demi baris dari file tabel dan menyalinnya ke file sementara, kecuali untuk baris pertama yang diabaikan. Setelah loop, file tabel awal dihapus (remove) dan file sementara diubah namanya menjadi file tabel asli (rename).

    **Delete from table where**

        void deleteFromTableWhere(const char* tableName, const char* column, const char* value)

    Fungsi ini menghapus baris dari tabel dengan nama tableName berdasarkan kondisi column = value. Mirip dengan deleteFromTable, fungsi ini membuka file tabel dan file sementara. Baris pertama dari file tabel dicopy ke file sementara. Selanjutnya, loop membaca baris-baris berikutnya dan memeriksa nilai di kolom yang sesuai dengan parameter column. Baris yang memenuhi kondisi diabaikan dan sisanya disalin ke file sementara. Jumlah baris yang dihapus dihitung dan dilaporkan setelah operasi selesai.

    **Update Table**

        void updateTable(const char* tableName, const char* column, const char* val)

    Fungsi ini mengganti nilai dari suatu kolom (column) dengan nilai baru (val) di semua baris tabel dengan nama tableName. Seperti biasa, file tabel dan file sementara dibuka, dan baris pertama disalin ke file sementara. Dengan menggunakan nilai column, posisi kolom yang akan diupdate ditentukan. Selanjutnya, loop membaca baris-baris berikutnya dan mengganti nilai kolom yang sesuai dengan nilai baru. Setelah loop, file tabel asli dihapus dan file sementara diubah namanya menjadi file tabel asli.

    **updateFromTableWhere**

        void updateFromTableWhere(const char* tableName, const char* column, const char* value, const char* wColumn, const char* wValue)

    Fungsi ini mengganti nilai kolom (column) dengan nilai baru (value) di semua baris tabel dengan nama tableName yang memenuhi kondisi wColumn = wValue. Seperti biasa, file tabel dan file sementara dibuka, dan baris pertama disalin ke file sementara. Dengan menggunakan nilai column dan wColumn, posisi kolom yang akan diupdate dan posisi kolom untuk kondisi where ditentukan. Setelah itu, loop membaca baris-baris berikutnya dan mengganti nilai kolom yang sesuai dengan nilai baru jika memenuhi kondisi where. Setelah loop, file tabel asli dihapus dan file sementara diubah namanya menjadi file tabel asli.

    **Select from table**

        void selectFromTable(const char* tableName, char* columns)

    Fungsi ini digunakan untuk menampilkan hasil seleksi dari tabel dengan nama tableName. Fungsi membuka file tabel untuk dibaca dan membaca baris pertama (header) untuk mendapatkan informasi mengenai posisi kolom yang sesuai dengan parameter columns. Selanjutnya, fungsi membaca baris-baris berikutnya dan menampilkan nilai kolom yang diminta sesuai dengan parameter columns. Fungsi ini dapat menangani dua kasus:

    - Jika columns adalah "*", maka semua kolom akan ditampilkan.
    - Jika columns berisi nama kolom yang spesifik, hanya kolom-kolom tersebut yang ditampilkan.

    Fungsi ini mencoba untuk menangani lebih dari satu kolom yang dipisahkan oleh koma (,). Selain itu, perlu diingat bahwa metode ini bergantung pada urutan kolom dalam file tabel.

    **Signal Handler**

        void signalHandler(int signal)
    
    Fungsi ini merupakan handler untuk sinyal SIGCHLD, yang biasanya dihasilkan ketika proses anak selesai dieksekusi. Fungsi ini menggunakan fungsi waitpid untuk menunggu selesainya proses anak, dan kemudian menampilkan pesan bahwa pengguna dengan nama yang disimpan dalam variabel uName telah terputus dari server.

    Sinyal SIGCHLD terjadi ketika proses anak selesai dieksekusi. Dalam hal ini, proses anak mungkin merupakan klien yang terputus dari server.

    **Select From Table Where**

        void selectFromTableWhere(const char* tableName, char* columns, const char* wColumn, const char* wValue)

    Fungsi selectFromTableWhere digunakan untuk mengambil dan menampilkan data dari suatu tabel berdasarkan kondisi tertentu. Fungsi ini memungkinkan pemilihan kolom yang ingin ditampilkan (columns), penentuan kondisi seleksi berdasarkan nilai pada suatu kolom (wColumn dan wValue), dan kemampuan menampilkan hasil seleksi. Jika parameter columns berisi "*", semua kolom akan ditampilkan, sedangkan jika berisi nama kolom tertentu, hanya kolom-kolom tersebut yang akan ditampilkan. Hasil seleksi akan diprint ke layar konsol.

    **Drop Column From Table**

        void dropColumnFromTable(const char* tableName, const char* column)

    digunakan untuk menghapus suatu kolom dari sebuah tabel. Fungsi ini membuka file tabel, mencari posisi kolom yang ingin dihapus, dan kemudian membuat file sementara tanpa kolom tersebut. Setelah itu, file sementara ini menggantikan file asli tabel.

    **Append Permission**

        void append_permission(const char *uname, const char *permission)

    digunakan untuk menghapus suatu kolom dari sebuah tabel. Fungsi ini membuka file tabel, mencari posisi kolom yang ingin dihapus, dan kemudian membuat file sementara tanpa kolom tersebut. Setelah itu, file sementara ini menggantikan file asli tabel.

    **Handle Client**

        void handleClient(int clSocket) {
    
    Fungsi handleClient adalah inti dari server yang menghandle setiap perintah yang diterima dari klien. Fungsi ini menggunakan serangkaian if-else untuk memproses berbagai jenis perintah SQL yang dapat diterima oleh server. Di bawah ini adalah penjelasan singkat tentang setiap blok perintah yang dihandle:

        CREATE DATABASE:
        Membuat direktori baru dengan nama database yang diterima dari klien.

        CREATE USER:
        Membaca nama pengguna (username) dan kata sandi (password) dari perintah.
        Menambahkan informasi pengguna ke file "user_list.txt" di direktori "databases/users".
        
        GRANT PERMISSION:
        Membaca nama database dan nama pengguna dari perintah.
        Menambahkan izin akses ("access") untuk pengguna ke dalam file "user_list.txt".
        
        DROP DATABASE:
        Menghapus direktori database dan seluruh isinya.
    
        USE:
        Mengganti direktori kerja (basePath) ke direktori database yang diinginkan.
        
        DROP TABLE:
        Menghapus file tabel dari direktori database saat ini.
        
        DROP COLUMN:
        Menghapus kolom dari suatu tabel di direktori database saat ini.
        
        CREATE TABLE:
        Membaca nama tabel dan definisi kolom dari perintah.
        Membuat file tabel baru dengan kolom yang didefinisikan.
        
        INSERT INTO:
        Menambahkan baris baru ke tabel dengan nilai-nilai yang diberikan.
    
        DELETE FROM:
        Menghapus baris dari tabel berdasarkan kondisi tertentu.
        
        UPDATE:
        Memperbarui nilai-nilai dalam baris tabel berdasarkan kondisi tertentu.
        
        SELECT:
        Menampilkan data dari tabel berdasarkan kolom dan kondisi tertentu.

    Setiap blok perintah SQL dilengkapi dengan pemanggilan fungsi dan tulisan log yang mencatat aktivitas yang dilakukan oleh pengguna. Kode ini sangat mengandalkan pola dan format perintah SQL yang masuk untuk memutuskan tindakan apa yang harus diambil. Selain itu, server memberikan respon ke klien setiap kali menerima perintah.

    **Program inti**

        int srvSocket, clSocket;
        struct sockaddr_in srvAddress, clAddress;
        socklen_t addrSize;
        pid_t chPID;

    - srvSocket dan clSocket: Mendefinisikan soket untuk server dan klien.
    - srvAddress dan clAddress: Struktur untuk menyimpan alamat server dan klien.
    - addrSize: Ukuran alamat.
    - chPID: Untuk menyimpan ID proses (Process ID).

        signal(SIGCHLD, signalHandler);

    Menetapkan fungsi signalHandler untuk menangani sinyal SIGCHLD (sinyal anak telah selesai).    

        srvSocket = socket(AF_INET, SOCK_STREAM, 0);

    Membuat soket menggunakan protokol IPv4 (AF_INET) dan tipe soket stream (SOCK_STREAM).

        if (bind(srvSocket, (struct sockaddr*)&srvAddress, sizeof(srvAddress)) == -1) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
        }

    Mengikat (binding) soket server dengan alamat dan port tertentu.

        if (listen(srvSocket, MAX_CLIENTS) == -1) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
        }

    Mengizinkan server untuk mendengarkan koneksi dari klien.

        clSocket = accept(srvSocket, (struct sockaddr*)&clAddress, &addrSize);

    Menerima koneksi dari klien.

        chPID = fork();
    
    Membuat proses anak untuk menangani klien.

## OUTPUT

![1](/uploads/290129ea09e8ec0f226ef3ce5a1d8059/1.png)
membuat user baru

![2](/uploads/db7da8a9c5ca2da3e2845a9eb1686332/2.png)
berhasil membuat user baru

![3](/uploads/7b235e41e2e100bd55e60bf24b3902c1/3.png)
setiap ada command baru, dibackup ke sini

![4](/uploads/09abde93ca68dd4872de8bd3514c7123/4.png)
user list beserta passwordnya

![5](/uploads/caa7909fe3b66b586cbb4afe4869235b/5.png)
log yang menampilkan timestamp

![6](/uploads/4887cd90f8db42bfcef9059e061bfbe3/6.png)
membuat database baru

![1](/uploads/f05823ba1a766d3e2a09c5d1e3a2ec46/1.png)
melakukan USE dan GRANT PERMISSION

![2](/uploads/fd4ec593002e391f4652da707b68ae39/2.png)
sukses melakukan USE dan GRANT PERMISSION

![1](/uploads/c9fc06e532bbcdfb6512547d47bb59f8/1.png)
melakukan beberapa command DDL

![2](/uploads/b27819c14d83a7bec868b7afad90fa61/2.png)
command DDL ada yang berhasil dan tidak

![1](/uploads/b888683299831e99a2f12de8e3c7363b/1.png)
melakukan beberapa command DML

![2](/uploads/ea65d514b24c8cf7e77675205962b571/2.png)
command DML ada yang berhasil dan tidak

![errorhandling](/uploads/ec532afad9741ab3c3cd8f3a4a22d37b/errorhandling.png)

Program bisa error handling (mengeluarkan pesan error tanpa keluar dari program client)

## REVISI
- program sudah bisa jalan (waktu demo gabisa sama sekali soalnya emang belum jadi (kami lelah))

## KENDALA

- tidak bisa melakukan DROP
- log tidak menampilkan user (hanya root)
- tidak bisa beberapa DML (delete, select)
- tidak bisa poin reliability (poin f di soal)
- tidak bisa poin tambahan (poin g di soal)

# PENUTUP
**Terima kasih banyak buat mas & mbak aslab sisop :))**

![tengs](gambar/tengs.jpg)




gambaran kami tiap mengerjakan sisop:

![meh](gambar/meh.jpg)
